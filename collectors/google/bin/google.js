#!/usr/bin/env node

var path = require('path');
var GoogleHook = require(path.join(__dirname, '../lib/google')).GoogleHook;

var myhook = new GoogleHook({
	name: "google-hook",
	debug: true,
	autoheal: true
});

myhook.start();

/* End of file google.js */
/* Location: ./google/bin/google.js */
