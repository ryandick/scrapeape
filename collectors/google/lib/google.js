var Hook = require('hook.io').Hook;
var util = require('util');
var conf = require('node-conf');
var consts = require('consts');
var fs = require('fs');
//	mongo db stuff
var mongo = require('mongoskin');
var mongodb = mongo.db('localhost:27017/scrapeape?auto_reconnect');
var mongoAccess = {
	insert: function(data, callback) {
		mongodb.collection('actions').save(data, {
			safe: true
		}, callback);
	},
	update: function(query, data, callback) {
		mongodb.collection('actions').update(query, data, {
			safe: true
		}, callback);
	},
	findOne: function(query, callback) {
		if (!query) {
			callback('no query provided for find', null);
		} else {
			mongodb.findOne(query, callback);
		}

	}
};
//	----------------------------------------------- //

function guidGenerator() {
	var S4 = function() {
			return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
		};
	return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}
var GoogleHook = exports.GoogleHook = function(options) {
		var guid = guidGenerator();


		Hook.call(this, options);

		// Load configuration
		this.conf = conf.load(consts.ENV)['google'];

		// Bind event listeners
		this.on('hook::ready', function() {
			//	on begin new query
			this.on('*::search-begin', function(beginData) {
				mongoAccess.insert({
					options: options,
					atStart:beginData,
					timestamp: {
						queued: new Date().toString(),
						lastEvent: new Date().toString(),
						completed: null
					},
					aid: beginData.aid,
					results: [],
					state: 'waiting',
					type: 'google-query'
				}, function(err, savedO) {
					if (err) throw err;
				});
			});
			//	on result found
			this.on('*::search-result', function(data) {
				mongoAccess.update({
					aid: guid
				}, {
					$set: {
						state: 'running',
						"timestamp.lastEvent": new Date().toString()
					},
					$push: {
						results: data
					}
				}, function(err, updated) {
					if (err) throw err;

				});
			}.bind(this));

		}.bind(this));
	};

util.inherits(GoogleHook, Hook);