#!/usr/bin/env node

var SocialHook = require('../lib/social').SocialHook;

var myhook = new SocialHook({
	name: "social-hook",
	debug: true,
	autoheal: true
});

myhook.start();

/* End of file social.js */
/* Location: ./social/bin/social.js */
