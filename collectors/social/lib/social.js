
var Hook    = require('hook.io').Hook;
var util    = require('util');
var conf    = require('node-conf');
var consts  = require('consts');

var SocialHook = exports.SocialHook = function(options) {
	Hook.call(this, options);
	
	// Load configuration
	this.conf = conf.load(consts.ENV)['social'];
	
	// Bind event listeners
	this.on('hook::ready', function() {
		
		/**
		 * Event listener
		 *
		 * @param   function  callback
		 */
		this.on('*::foo', function(callback) {
			
		}.bind(this));
		
	}.bind(this));
};

util.inherits(SocialHook, Hook);

/**
 * Do something
 * 
 * @access  public
 * @return  void
 */
SocialHook.prototype.doSomething = function() {
	
};

/* End of file social.js */
/* Location: ./social/lib/social.js */
