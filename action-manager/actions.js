module.exports = {
	create: function(actionType, actionOps, callback) {
		if ((actionType) && (actionOps)) {
			require('./db.js').actions.save(actionType, actionOps, callback);
		} else {
			callback('missing type or options', null);
		}
	},
	markDone: function(aid, callback) {
		if (aid) {
			require('./db.js').actions.markDone(aid, callback);
		} else {
			callback('action id not supplied', null);
		}
	}
};