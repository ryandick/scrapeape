var path = require('path');
module.exports = {
	utils: {
		genGuid: function() {
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random() * 16 | 0,
					v = c == 'x' ? r : (r & 0x3 | 0x8);
				return v.toString(16);
			});
		}
	},
	google: {
		runQuery: function(query, aid, callback) {
			var actions = require(path.join(__dirname,'./actions.js'));
			var timestamp = new Date().toString();
			actions.create('google-scrape', {
				aid: aid,
				query: query,
				timestamp: timestamp
			}, function(err, result) {
				if (!err) {
					require(path.join(__dirname,'./proc.js')).google.runQuery(aid, query);
					callback({
						msg: "Google Scrape Ape dispatched!",
						aid: aid,
						query:query,
						result:result
					});
				} else {
					callback({
						msg: 'failed to create queue entry.',
						hasErr: true,
						query:query,
						aid:aid
					});
				}
			});
		}
	}
};