//	mongo db stuff
var mongo = require('mongoskin');
var mongodb = mongo.db('localhost:27017/scrapeape?auto_reconnect');
var mongoAccess = {
	insert: function(object, callback) {
		mongodb.collection('queue').save(object, {
			safe: true
		}, callback);
	},
	update: function(query, object, callback) {
		mongodb.collection('queue').update(query, object, {
			safe: true
		}, callback);
	}
};
//	----------------------------------------------- //
module.exports = {
	actions: {
		save: function(actionType, actionOps, callback) {
			mongoAccess.insert({
				type: actionType,
				options: actionOps,
				timestamp: new Date().toString(),
				active: true,
				aid: actionOps.aid
			}, callback);
		},
		markDone: function(aid, callback) {
			mongoAccess.update({
				aid: aid
			}, {
				$set: {
					active: true
				}
			}, callback);
		}
	}
};

