var path = require('path');
var forever = require('forever-monitor');
module.exports = {
	google: {
		runQuery: function(aid, query) {
			if (!query) {
				callback('no query provided', null);
			} else {
				var child = forever.start([ path.join(__dirname,'../providers/google/app.js'), query, aid], {
					max: 2,
					silent: false,
					uid: aid
				});
				child.on('stderr', function(data) {
				});
			}
		}
	}
};