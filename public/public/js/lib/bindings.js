//	control and hash change event bindings
page.bindings = {
	//	bind entire page
	bind: {
		index: function() {
			//	run search with query now
			jQuery.hashListen('/query/now/', function() {
				jQuery.post('/query/now/', {
					query: $('#gQuery').val(),
					timestamp: new Date().toString()
				}, function(data, textStatus, xhr) {

				});
			});
			jQuery.hashListen('/index/', function() {
				jQuery.get('/assets/mustaches/index.tmpl.html', function(indexHtml) {
					$('.fluid-container').html(indexHtml);
				});
			});
			//	bind controls
			page.bindings._private.controls.index();
		}
	},
	_private: {
		controls: {
			index: function() {
				jQuery(document).on('click', '.searchQuery', function(e) {
					e.preventDefault();
					window.location = "/#/query/now/";
				});
			}
		}
	}

};