// *    settings
var config = require('./config.js');
// deps
var path = require('path');

// *  Expose Site

module.exports = Site;


// * init.
function Site(options) {
  //options
  this.options = options || {port:8000,  bind:"localhost", publicDir:path.resolve("out/")};
}
//* http server
Site.prototype.server = require('./providers/http/index.js');
