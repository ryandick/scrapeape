var Site = require('./lib.js');
var site = new Site();
var actionManager = require('../../action-manager/manager.js');
site.server.start(site.options, function(err, app) {
	if (err) throw err;
	else {
		//	run search query right now
		app.post('/query/now/', function(req, res) {
			if (req.body['query']) {
				//	spawn process
				var aid =actionManager.utils.genGuid();
				actionManager.google.runQuery(req.body['query'],aid, function(result) {
					result.aid = aid;
					res.json(result);
				});
			} else {
				res.json({
					msg: 'did not receive a query string.',
					hasErr: true
				});
			}
		});
		//	start express server
		app.listen(site.options.port);
		console.log('HTTP server Listening On: http://localhost:' + site.options.port);
	}
});