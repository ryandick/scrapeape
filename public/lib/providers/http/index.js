/**
 * Exports
 */
exports.start = function(options, callback) {
  /**
   * Module dependencies.
   */
  var express = require('express'),
    http = require('http'),
    path = require('path');
    app = express();
  /**
   * Setup
   */
  app.configure(function() {
    app.set('port', options.port);
    app.set('views', __dirname + '/views');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(express.static(options.publicDir));
  });
  /**
   * Config
   */
  app.configure('development', function() {
    app.use(express.errorHandler());
  });
  /**
   * Routes
   */
   
  callback(null, app);
 };
