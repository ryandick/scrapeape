var Hook = require('hook.io').Hook;
var program = require('commander');

program
  .version('0.0.1')
  .parse(process.argv);

var hook = new Hook({
	name: 'socialstat',
	debug: true,
	autoheal: true
});

hook.on('hook::ready', function() {
	var url = program.args;
	require('./lib.js').socStat(url, function(result) {
		hook.emit('social-results', {
			results: result,
			url: 'http://google.com'
		});
	});
});
hook.start();