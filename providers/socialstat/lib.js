 var request = require('request');

 var social = new Object;
 social = [  
     ['YCombinator'  , 'http://api.thriftdb.com/api.hnsearch.com/items/_search?q='           , /points": (\d*),/],
     ['Facebook'     , 'http://api.facebook.com/restserver.php?method=links.getStats&urls='  , /<share_count>(\d*)<\/share_count>/],
     ['Google+'      , 'https://plusone.google.com/u/0/_/+1/fastbutton?url='                 , /window.__SSR = {c: (\d*).0 ,/],
     ['Twitter'      , 'http://urls.api.twitter.com/1/urls/count.json?url='                  , /count":(\d*),/],
     ['LinkedIn'     , 'http://www.linkedin.com/countserv/count/share?url='                  , /count":(\d*),/],
     ['Dzone'        , 'http://widgets.dzone.com/links/widgets/zoneit.html?t=1&url='         , />(\d*)<\/a>/],
     ['Digg'         , 'http://widgets.digg.com/buttons/count?url='                          , /diggs": (\d*)/],
     ['Delicious'    , 'http://feeds.delicious.com/v2/json/urlinfo/data?url='                , /total_posts": (\d*)/]
 ];
exports.socStat = function(url,cb){
var resultSet = [];
var i = 0;
 social.forEach(function(val) {
     var headers = {'user-agent': 'node.js'};
     request({url:url, headers:headers}, function(error, response, body) {
        var reg= new RegExp(val[2]);
        var match=reg.exec(body);
        if(match && match[1]!==''){
		var serviceName = val[0];
		var matches = match[1];
		resultSet.push({service:serviceName, marks:matches})    ;   }
        else {
            resultSet.push({service:val[0], marks:0});
        }
i++;
if(i >= social.length){
cb(resultSet);
}
});
});
};
