#!/usr/bin/env node

/**
 * Module dependencies.
 */

var program = require('commander');

program.version('0.0.1').parse(process.argv);

//console.log('GOoglescrAPErGO, searching for: '+program.args);
var google = require('./lib/google.js');
var hookio = require('hook.io').Hook;
var nextCounter = 0;
var hook = new hookio({
  name: "google-emmiter",
  autoheal: true,
  debug: true
});
hook.on('hook::ready', function() {
  //  notify collectors of new search
  hook.emit('search-begin', {
    timestamp: new Date().toString(),
    aid: program.args[1],
    query: program.args[0]
  });
  console.log(JSON.stringify(program));
  google(program.args[0], function(err, next, links) {
    if (err) {
      hook.emit('search-error', {
        err: err
      });
    } else {
      hook.emit('search-result', {
        links: links,
        date: new Date(),
        query: program.args[0],
        aid: program.args[1]
      });
      if (nextCounter < 50) {
        nextCounter += 1;
        if (next) {
          next();
        }
      } else {
        //  notify collectors of end of search
        hook.emit('search-end', {
          timestamp: new Date().toString(),
          aid: program.args[1],
          query: program.args[0]
        });
      }

    }
  });
});
hook.start();