#!/usr/bin/env node

var cp = require('child_process');
var path = require('path');
var winston = require('winston');
var log = new(winston.Logger)({
	transports: [
	new(winston.transports.Console)({
		colorize: true,
		timestamp: true
	}), new(require('winston-mongodb').MongoDB)({
		db: 'scrapeape',
		collection: 'logs'
	})]
});
log.cli();
// A list of all spawned processes
var processes = [];

// First, we spawn the core
// Then spawn the various child hooks (after waiting a little while
// to make sure the core hook has started..)
var conf = require(path.join(__dirname, 'providers.desc.json'));
if (conf) log.info('config loaded');
else log.error('config failed to load');
conf.hooks.forEach(

function(hook) {
	if (conf.disabled.indexOf(hook) < 0) {
		spawn('collectors/' + hook + '/bin/' + hook + '.js');
	}
});


// Make sure we clean up when exiting
process.on('SIGTERM', function() {
	processes.forEach(function(proc) {
		log.warn('killing PID: ' + proc.pid);
		proc.kill(0);
	});
});

// Spawns a new process


function spawn(bin, outputCallback) {
	log.info('Spawning: ' + __dirname + '/' + bin);
	var proc = cp.spawn(path.join(__dirname, bin));
	if (outputCallback) {
		var callback = outputCallback;
		outputCallback = function() {
			callback();
			callback = null;
			outputCallback = function() {};
		};
		proc.stdout.once('data', function() {
			outputCallback();
		});
		proc.stderr.once('data', function() {
			outputCallback();
		});
	}
	proc.stdout.on('data', function(data) {
		log.info(data);

	});
	proc.stderr.on('data', function(data) {
		log.error(data);
	});
	processes.push(proc);
	log.info('---> Spawned!');
	log.info('---> Spawn Path: ' + __dirname + '/' + bin);
	log.info('---> PID: ' + proc.pid);
}